# 2D Mobile Platformer
## Juego incompleto desarrollado en Unity con C#. A futuro será un "endless runner" en 2D para Android.
## Para aprender lo necesario, se está siguiendo el tutorial de AbleGamesDev. Link del canal: https://www.youtube.com/channel/UCBrCPYAEhVCjYJJj1SMyzvA
### Instrucciones:
### 1) En el menú de la izquierda, hacer click en Downloads.
### 2) Hacer click en Download repository.
### 3) Una vez que se descargue el archivo .zip, descomprimirlo con WinRAR, WinZip u otro software de preferencia.
### 4) Importar la carpeta extraída a Unity para editar el proyecto, o bien, copiar el archivo APK al dispositivo Android donde se quiera jugar el videojuego.
