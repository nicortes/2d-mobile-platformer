﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO LO QUE SEA public, ES VISIBLE EN UNITY

// La clase PlayerManager hereda los métodos de la clase por defecto MonoBehaviour (Start y Update)
public class PlayerManager : MonoBehaviour {

    public float speedX;
    public float jumpSpeedY;
    bool facingRight;
    bool jumping;
    float speed;
    bool wasd;

    // Se declara una variable de tipo Animator (Animator es el nombre de uno de los componentes del objeto Jugador)
    Animator animator;
    Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        facingRight = true;
    }

    // Update is called once per frame
    void Update() {

        // Controlador de movimiento
        MovePlayer(speed);
        // Controlador de salto
        JumpPlayer(KeyCode.W);
        
    }
    
    // Mover
    void MovePlayer(float speed){
        // Movimiento con cierto momentum, primero parte despacio
        //rb.AddForce(new Vector3(speed, rb.velocity.y, 0));

        // Buena forma de mover, pero no me deja saltar:
        //rb.MovePosition(rb.position + new Vector2(speed, rb.velocity.y) * Time.deltaTime);

        // ARREGLAR EN QUÉ DIRECCIÓN MIRA CUANDO SE APRETAN A Y D AL MISMO TIEMPO
        // LA DIRECCIÓN POR DEFECTO ES LA ÚLTIMA FUNCIÓN QUE SE LLAME
        // EN ESTE CASO, COMO LA ÚLTIMA TIENE ARGUMENTO A, QUEDA MIRANDO HACIA LA IZQUIERDA

        KeyCode left_key;
        KeyCode right_key;

        left_key = KeyCode.A;
        right_key = KeyCode.D;

        // Movimiento con teclado
        Mover(left_key, right_key);

        DejarDeMover(left_key, right_key);

        //DejarDeMover(KeyCode.D);
        //DejarDeMover(KeyCode.A);

        FlipSprite();
        
        rb.velocity = new Vector3(speed, rb.velocity.y, 0);
    }

    // Salto con teclado
    void JumpPlayer(KeyCode jump) {
        if (Input.GetKeyDown(jump)) {
            Jump();
        }
    }

    // Salto con pantalla táctil
    public void Jump() {
        // Saltar
        jumping = true;
        rb.AddForce(new Vector2(rb.velocity.x, jumpSpeedY));
        animator.SetInteger("State", 3);
    }

    // Moverse a la derecha con pantalla táctil
    public void GoRight() {
        speed = speedX;
        if (jumping == false) {
            animator.SetInteger("State", 1);
        }
    }

    // Moverse a la izquierda con pantalla táctil
    public void GoLeft() {
        speed = -speedX;
        if (jumping == false) {
            animator.SetInteger("State", 1);
        }
    }

    public void StopMoving() {
        speed = 0;
        if (jumping == false) {
            animator.SetInteger("State", 0);
        }
    }

    // Movimiento horizontal con teclado
    void Mover(KeyCode left, KeyCode right) {
        // Si presiona la tecla para moverse a la izquierda
        if (Input.GetKey(left)){
            GoLeft();
        }
        // Si presiona la tecla para moverse a la derecha
        if (Input.GetKey(right)) {
            GoRight();
        }
    }

    // Si suelta la tecla, o si se intenta mover a la derecha y a la izquierda al mismo tiempo, se detiene al jugador
    void DejarDeMover(KeyCode left, KeyCode right){
        if (Input.GetKeyUp(left) || Input.GetKeyUp(right) || (Input.GetKey(left) && Input.GetKey(right))) {
            StopMoving();
        }
        /* Para jugar con flechas del teclado y con WASD
         * Sólo deja de caminar si se suelta una tecla y no se está presionando la otra
        if ( ( Input.GetKeyUp(one) && !Input.GetKey(two) ) || ( Input.GetKeyUp(two) && !Input.GetKey(one) ) ){
            speed = 0;
            if (jumping == false){
                animator.SetInteger("State", 0);
            }
        }
        */
    }

    private void OnCollisionEnter2D(Collision2D collision){
        if(collision.gameObject.tag == "GroundObject" || collision.gameObject.tag == "CrateObject"){
            jumping = false;
            animator.SetInteger("State", 0);
        }
    }

    // Voltea horizontalmente el sprite del jugador
    void FlipSprite() {
        // Si se mueve en una dirección, pero está mirando en dirección opuesta, lo hace mirar al lado correcto
        // Si está presionando ambas direcciones, se queda con la dirección que tenía antes de quedarse quieto
        if ((speed < 0 && facingRight == true || speed > 0 && facingRight == false) && 
            (!(Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D)))) {
            // Si estaba en true, ahora es false, y viceversa
            facingRight = !facingRight;
            // Se guarda la propiedad Scale del componente Transform en una variable temporal
            Vector3 temp = transform.localScale;
            // Se cambia el signo del eje x (positivo: normal, negativo: invertido)
            temp.x = -temp.x;
            // Se aplica el cambio al objeto
            transform.localScale = temp;
        }
    }

}
